# Mercury

One Paragraph of project description goes here

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

Using Swift Package

```
.package(url: "https://gitlab.com/thecb4/Mercury.git", .upToNextMinor(from: "0.1.0"))
```

Using Carthage

```
git "https://gitlab.com/thecb4/Mercury.git" ~> 0.1.0
```

## Using
```swift
let console = Console()
console.asynchronously = false
Mercury.shared.add(destination: Console())
Mercury.shared.debug("hello world")
```


## Built With

* [DarkMatter](https://gitlab.com/thecb4/DarkMatter) - Extensions on Codable
* [HyperSpace](https://gitlab.com/thecb4/HypeSpace) - Strongly typed API access


## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Cavelle Benjamin** - *Initial work* - [TheCB4](https://thecb4.io)

See also the list of [contributors](https://gitlab.com/thecb4/Mercury/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

