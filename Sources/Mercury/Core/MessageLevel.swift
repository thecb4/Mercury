import Foundation
import DarkMatter

public struct MessageLevel: DarkMatter {
  
  public var rawValue: Int
  
  public var string: String
  
  public init(rawValue: Int, string: String) {
    self.rawValue = rawValue
    self.string   = string
  }
  
}

extension MessageLevel {
  public init() {
    rawValue = 4
    string = "VERBOSE"
  }
}

/*
 0  Emergency  emerg  panic[7]  System is unusable  A panic condition.[8]
 1  Alert  alert    Action must be taken immediately  A condition that should be corrected immediately, such as a corrupted system database.[8]
 2  Critical  crit    Critical conditions  Hard device errors.[8]
 3  Error  err  error[7]  Error conditions
 4  Warning  warning  warn[7]  Warning conditions
 5  Notice  notice    Normal but significant conditions  Conditions that are not error conditions, but that may require special handling.[8]
 6  Informational  info    Informational messages
 7  Debug  debug    Debug-level messages  Messages that contain information normally of use only when debugging a program.[8]
 */

extension MessageLevel {
  public static var verbose = MessageLevel(rawValue: 8, string: "VERBOSE")
  public static var debug   = MessageLevel(rawValue: 7, string: "DEBUG")
  public static var info    = MessageLevel(rawValue: 6, string: "INFO")
  public static var warn    = MessageLevel(rawValue: 4, string: "WARN")
  public static var error   = MessageLevel(rawValue: 3, string: "ERROR")
}