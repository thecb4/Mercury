import Foundation

public final class Mercury: Messenger, DestinationManager {
  
  var destinations: Set<DestinationBox>
  
  public required init() {
    self.destinations = Set<DestinationBox>()
  }
  
  public static let shared: Messenger = Mercury()

}