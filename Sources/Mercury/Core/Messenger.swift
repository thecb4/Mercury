import Foundation

public protocol Messenger: class {

  // var destinations: Set<DestinationBox> { get set }

  static var shared: Messenger { get }

  init()

  @discardableResult
  func add(destination: Destination) -> Bool

  @discardableResult
  func remove(destination: Destination) -> Bool

  func createPackage(_ level: MessageLevel, message:  @escaping @autoclosure () -> Any, timestamp: Date, file: String, line: Int, column: Int, function: String, thread: String) -> MessagePackage

  func verbose(_ message: @escaping @autoclosure () -> Any, timestamp: Date, file: String, line: Int, column: Int, function: String, thread: Thread)

  func debug(_ message: @escaping @autoclosure () -> Any, timestamp: Date, file: String, line: Int, column: Int, function: String, thread: Thread)

  func info(_ message:  @escaping @autoclosure () -> Any, timestamp: Date, file: String, line: Int, column: Int, function: String, thread: Thread)

  func warn(_ message:  @escaping @autoclosure () -> Any, timestamp: Date, file: String, line: Int, column: Int, function: String, thread: Thread)

  func error(_ message: @escaping @autoclosure () -> Any, timestamp: Date, file: String, line: Int, column: Int, function: String, thread: Thread)

  func custom(level: MessageLevel, _ message: @escaping @autoclosure () -> Any, timestamp: Date, file: String, line: Int, column: Int, function: String, thread: Thread)

}

extension Messenger {

  public func verbose(_ message:  @escaping @autoclosure () -> Any, timestamp: Date = Date(), file: String = #file, line: Int = #line, column: Int = #column, function: String = #function, thread: Thread = Thread.current) {
    guard let manager = self as? DestinationManager else { return }
    manager.send(.verbose, message: message, timestamp: timestamp, file: file, line: line, column: column, function: function, thread: "\(thread)")
  }

  public func debug(_ message:   @escaping @autoclosure () -> Any, timestamp: Date = Date(), file: String = #file, line: Int = #line, column: Int = #column, function: String = #function, thread: Thread = Thread.current) {
    guard let manager = self as? DestinationManager else { return }
    manager.send(.debug, message: message, timestamp: timestamp, file: file, line: line, column: column, function: function, thread: "\(thread)")
  }

  public func info(_ message:   @escaping @autoclosure () -> Any, timestamp: Date = Date(), file: String = #file, line: Int = #line, column: Int = #column, function: String = #function, thread: Thread = Thread.current) {
    guard let manager = self as? DestinationManager else { return }
    manager.send(.info, message: message, timestamp: timestamp, file: file, line: line, column: column, function: function, thread: "\(thread)")
  }

  public func warn(_ message:   @escaping @autoclosure () -> Any, timestamp: Date = Date(), file: String = #file, line: Int = #line, column: Int = #column, function: String = #function, thread: Thread = Thread.current) {
    guard let manager = self as? DestinationManager else { return }
    manager.send(.warn, message: message, timestamp: timestamp, file: file, line: line, column: column, function: function, thread: "\(thread)")
  }

  public func error(_ message:   @escaping @autoclosure () -> Any, timestamp: Date = Date(), file: String = #file, line: Int = #line, column: Int = #column, function: String = #function, thread: Thread = Thread.current) {
    guard let manager = self as? DestinationManager else { return }
    manager.send(.error, message: message, timestamp: timestamp, file: file, line: line, column: column, function: function, thread: "\(thread)")
  }

  public func custom(level: MessageLevel, _ message:   @escaping @autoclosure () -> Any, timestamp: Date = Date(), file: String = #file, line: Int = #line, column: Int = #column, function: String = #function, thread: Thread = Thread.current) {
    guard let manager = self as? DestinationManager else { return }
    manager.send(level, message: message, timestamp: timestamp, file: file, line: line, column: column, function: function, thread: "\(thread)")
  }

  public func createPackage(_ level: MessageLevel, message:  @autoclosure () -> Any, timestamp: Date = Date(), file: String = #file, line: Int = #line, column: Int = #column, function: String = #function, thread: String) -> MessagePackage {

    return MessagePackage(
      app: "Mercury",
      level: level.rawValue,
      message: "\(message())",
      timestamp: timestamp,
      file: file,
      line: line,
      column: column,
      function: function,
      thread: thread
    )

  }

}
