import Foundation

internal protocol DestinationManager: class {
  var destinations: Set<DestinationBox> { get set }

  @discardableResult
  func add(destination: Destination) -> Bool
  
  @discardableResult
  func remove(destination: Destination) -> Bool

  func send(_ level: MessageLevel, message:  @escaping @autoclosure () -> Any, timestamp: Date, file: String, line: Int, column: Int, function: String, thread: String)

}

extension DestinationManager {

  @discardableResult
  public func add(destination: Destination) -> Bool {
    if destinations.contains(DestinationBox(destination)) {
      return false
    }
    destinations.insert(DestinationBox(destination))
    return true
  }
  
  @discardableResult
  public func remove(destination: Destination) -> Bool {
    if destinations.contains(DestinationBox(destination)) == false {
      return false
    }
    destinations.remove(DestinationBox(destination))
    return true
  }

  // https://stackoverflow.com/questions/40764140/operationqueue-main-vs-dispatchqueue-main
  public func send(_ level: MessageLevel, message:  @escaping @autoclosure () -> Any, timestamp: Date, file: String, line: Int, column: Int, function: String, thread: String) {
    
    for box in destinations {
      
      let destination = box.destination
      
      guard let queue = destination.queue else { fatalError("Queue not set") }
      
      let shouldSend = destination.shouldSend(level)
      
      switch (destination.asynchronously, shouldSend) {
        case (true,  true):
          queue.async {
            guard let packager = self as? Messenger else { return }
            let package = packager.createPackage(level, message: message, timestamp: timestamp, file: file, line: line, column: column, function: function, thread: thread)
            destination.send(package)
          }
        case (false, true):
          queue.sync  {
            guard let packager = self as? Messenger else { return }
            let package = packager.createPackage(level, message: message, timestamp: timestamp, file: file, line: line, column: column, function: function, thread: thread)
            destination.send(package)
          }
        default: return
      }
      
    }
    
  }

}