import Foundation

public protocol Destination {
  
  var identifier: UUID { get }
  var level: MessageLevel { get set }
  var queue: DispatchQueue? { get }
  var asynchronously: Bool { get set }
  func shouldSend(_ level: MessageLevel) -> Bool
  func send(_ package: MessagePackage)
  init()
  init(identifier: UUID, queue: DispatchQueue, asynchronously: Bool, level: MessageLevel)
  
}

extension Destination {
  
  public func shouldSend(_ level: MessageLevel) -> Bool {
    return level.rawValue <= self.level.rawValue
  }
  
}