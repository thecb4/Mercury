import Foundation

// https://stackoverflow.com/questions/38067838/swift-array-reduction-cannot-use-mutating-member-on-immutable-value
extension Set {
  //returns a new set with the element inserted
  func inserting(_ element: Element) -> Set<Element> {
    var set = self
    set.insert(element)
    return set
  }
}