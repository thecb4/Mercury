import Foundation

internal struct DestinationBox: Hashable {
  
  let destination: Destination
  
  public var hashValue: Int {
    return destination.identifier.hashValue
  }
  
  public static func == (lhs: DestinationBox, rhs: DestinationBox) -> Bool {
    return lhs.hashValue == rhs.hashValue
  }
  
  init(_ destination: Destination) {
    self.destination = destination
  }
  
}