import Foundation
import DarkMatter

public struct MessagePackage {
  public let app: String
  public let level: Int
  public let message: String
  public let timestamp: Date
  public let file: String
  public let line: Int
  public let column: Int
  public let function: String
  public let thread: String
}

extension MessagePackage {
  init() {
    app = "NowThis"
    level = MessageLevel().rawValue
    message =  ""
    timestamp = Date()
    file =  ""
    line = -1
    column = -1
    function =  ""
    thread =  ""
  }
}

extension MessagePackage: DarkMatter { }