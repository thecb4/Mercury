import Foundation

public class Console: Destination {

  public private(set) var identifier: UUID
  
  public var level: MessageLevel
  
  public private(set) var queue: DispatchQueue?
  
  public var asynchronously: Bool
  
  public required init(identifier: UUID, queue: DispatchQueue, asynchronously: Bool, level: MessageLevel) {
    self.identifier = identifier
    self.queue = queue
    self.asynchronously = asynchronously
    self.level = level
  }

  public required init() {
    let id = UUID()
    self.identifier = id
    self.queue = DispatchQueue(label: "mercury-\(id.uuidString)", target: self.queue)
    self.asynchronously = true
    self.level = .verbose
  }
  
  public func send(_ package: MessagePackage) {
    print("\(package.timestamp) \(package.message)")
  }
  
}
