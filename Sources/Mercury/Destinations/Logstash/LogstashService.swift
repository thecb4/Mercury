import Foundation
import DarkMatter
import HyperSpace

protocol ServiceProtocolPrivate: class {
  init(using configuration: URLSessionConfiguration, sessionDelegate: APISessionDelegateProtocol, delegateQueue: OperationQueue)
}

public final class LogstashService: ServiceProtocol, ServiceProtocolPrivate {
  
  public private(set) var session: URLSessionProtocol
  
  public private(set) var sessionDelegate: APISessionDelegateProtocol
  
  public var tasks: [URLSessionTaskKey : URLSessionTaskProtocol] = [:]
  
  required init(
    using configuration: URLSessionConfiguration = URLSession.shared.configuration,
    sessionDelegate: APISessionDelegateProtocol = APISessionDelegate(),
    delegateQueue: OperationQueue = URLSession.shared.delegateQueue) {
    self.sessionDelegate = sessionDelegate
    self.session = URLSession(configuration: configuration, delegate: sessionDelegate, delegateQueue: delegateQueue)
  }
  
  public func registerSessionAuthenticationChallengeHandler<API>(for endPoint: EndPoint<API>) where API : APIProtocol {
    
    self.sessionDelegate.challengeHandlers[ endPoint.environment.value.host ] = endPoint.environment
    
  }
  
  public func make(mock: MockConnection? = nil, _ request: URLRequest, for taskKey: URLSessionTaskKey, completionHandler: @escaping DataTaskCompletionHandler) {
    
    if let mock = mock {
      
      completionHandler(mock.data, mock.response, mock.error)
      
    } else {
      
//      tasks[taskKey]?.cancel()
      
      tasks[taskKey] = session.dataTask(with: request) { (data, response, error) in
        
        defer { self.tasks[taskKey] = nil }
        
        DispatchQueue.main.async {
          
          completionHandler(data, response, error)
          
        }
        
      }
      
      tasks[taskKey]?.resume()
      
    }
    
  }

  public func connect<API: APIProtocol>(mock: MockConnection? = nil, to endPoint: EndPoint<API>, completionHandler: @escaping (Int?) -> Void) {
    
    if let mock = mock {

      guard let response = mock.response as? HTTPURLResponse else {
        completionHandler(nil)
        return
      }
      
      completionHandler( response.statusCode )
      
    } else {
      
      let taskKey = endPoint.route.taskKey
      
      guard let request = endPoint.request else {
        completionHandler(nil)
        return
      }
      
      make(request, for: taskKey) { (_, response, _) in

        guard let response = response as? HTTPURLResponse else {
          completionHandler(nil)
          return
        }
        
        completionHandler( response.statusCode )

      }
      
    }
    
  }
  
  public func connect<API: APIProtocol>(mock: MockConnection? = nil, to endPoint: EndPoint<API>, completionHandler: @escaping (Data?) -> Void) {
    
    if let mock = mock {
      
      completionHandler( mock.data )
      
    } else {
      
      let taskKey = endPoint.route.taskKey
      
      guard let request = endPoint.request else {
        completionHandler(nil)
        return
      }
      
      make(request, for: taskKey) { (data, _, _) in
        completionHandler(data)
      }
      
    }
    
  }
  
  public func connect<API: APIProtocol, Content: Codable>(mock: MockConnection? = nil, to endPoint: EndPoint<API>, returning: Content.Type, completionHandler: @escaping (Content?) -> Void)  {
    
    if let mock = mock {
      
      guard let data = mock.data, let content = try? data.decoded() as Content else {
        completionHandler(nil)
        return
      }
      
      completionHandler(content)
      
    } else {
      
      let taskKey = endPoint.route.taskKey
      
      guard let request = endPoint.request else {
        completionHandler(nil)
        return
      }
      
      make(request, for: taskKey) { (data, _, _) in
        
        guard let data = data, let content = try? data.decoded() as Content else {
          completionHandler(nil)
          return
        }
        
        completionHandler(content)
      }
      
    }
    
  }
  
  public static let shared: LogstashService = LogstashService()
  
}


extension LogstashService {
  
  func log(_ package: MessagePackage, completionHandler: @escaping (Data?) -> Void) {
    
    let endPoint = EndPoint<LogstashAPI>(at: .send(package))
    
    registerSessionAuthenticationChallengeHandler(for: endPoint)
    
    self.connect(to: endPoint) { (results) in
      completionHandler(results)
    }
    
  }
  
}
