import Foundation
import HyperSpace

// public typealias Environment = EnvironmentProtocol

public class Logstash: Destination {
  
  private let service = LogstashService.shared
  
  public required init() {
    let id = UUID()
    self.identifier = id
    self.queue = DispatchQueue(label: "mercury-\(id.uuidString)", target: self.queue)
    self.asynchronously = true
    self.level = .verbose
  }
  
  public required init(identifier: UUID, queue: DispatchQueue, asynchronously: Bool, level: MessageLevel) {
    self.identifier = identifier
    self.queue = queue
    self.asynchronously = asynchronously
    self.level = level
  }

  public convenience init(_ url: URL.Env) {

    LogstashAPI.current = .remote(url)

    self.init()

  }
  
  public convenience init(_ url: URL.Env, identifier: UUID, queue: DispatchQueue, asynchronously: Bool, level: MessageLevel) {

    LogstashAPI.current = .remote(url)

    self.init(
      identifier: identifier, 
      queue: queue, 
      asynchronously: asynchronously, 
      level: level
    )

  }
  
  public private(set) var identifier: UUID
  
  public var level: MessageLevel
  
  public private(set) var queue: DispatchQueue?
  
  public var asynchronously: Bool
  
  public func send(_ package: MessagePackage) {
    
    service.log(package) { _ in }
    
  }
  
}
