//
//  logstashAPI.swift
//  NowThis
//
//  Created by Cavelle Benjamin on 18-Dec-25 (52).
//  Copyright © 2018 The CB4. All rights reserved.
//

import Foundation
import HyperSpace

struct LogstashAPI: APIProtocol {

  enum Environment: EnvironmentProtocol {
    
    case remote(URL.Env)
    
    var value: URL.Env {
      switch self {
        case .remote(let env):
          return env
      }
    }
    
    func handle(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
      
      #if os(Linux)
        completionHandler(.performDefaultHandling, nil)
      #else 
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
      #endif

    }
    
  }
  
  static var current: Environment = .remote( URL.Env.localhost(80) )
  
  enum Route: RouteProtocol, URLSessionTaskable {
    
    case send(MessagePackage)
    
    var requestCachePolicy: URLRequest.CachePolicy {
      switch self {
      case .send:
        return .useProtocolCachePolicy
      }
    }
    
    var requestTimeoutInterval: TimeInterval {
      switch self {
      case .send:
        return 30.0
      }
    }
    
    var taskKey: URLSessionTaskKey {
      switch self {
      case .send: return URLSessionTaskKey(name: "logstash")
      }
    }
    
    var path: String {
      switch self {
      case .send(_):
        return "/"
      }
    }
    
    var queryItems: [URLQueryItem]? {
      switch self {
      case .send:
        return []
      }
    }
    
    var headers: [String: String]? {
      switch self {
      case .send:
        return ["Content-Type":"application/json"]
      }
    }
    
    var httpMethod: String {
      switch self {
      case .send: return "POST"
      }
    }
    
    var body: Data? {
      switch self {
      case .send(let package):
        return try? package.encoded()
      }
    }
    
    func mockHttpDataResponse(for statusCode: Int) -> Data? {
      
      switch self {
      case .send:
        
        return Data()
        
      }
      
    }
    
  }
  
}
