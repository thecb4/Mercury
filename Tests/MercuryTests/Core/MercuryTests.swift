import XCTest
import HyperSpace
import Mercury

final class MercuryTests: XCTestCase {
    func testConsole() {
        let destination = Console()
        destination.asynchronously = false
        let logger = Mercury.shared
        logger.add(destination: Console())
        logger.debug("hello world")
    }

    func testLogstash() {
        let destination = Logstash( URL.Env.localhost(1000) )
        destination.asynchronously = false
        let logger = Mercury.shared
        logger.add(destination: destination)
    }

    func testHiddenDestinationManagerParameters() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        // XCTAssertEqual(Mercury().text, "Hello, World!")
        let destination = Console()
        destination.asynchronously = false
        let logger = Mercury.shared
        logger.add(destination: Console())
        logger.debug("hello world")

        // Does not compile (good)
        // Mercury.shared.destinations = Set<DestinationBox>()

        // Does not compile (good)
        // Mercury.shared.destinations = Set<Int>()

        // Does not compile (good)
        // guard let manager = Mercury.shared as? DestinationManager else { return }
        // manager.destinations = Set<Int>()

        print("testing")
    }

    static var allTests = [
        ("testConsole", testConsole),
        ("testLogstash", testLogstash),
        ("testHiddenDestinationManagerParameters", testHiddenDestinationManagerParameters),
    ]
}
