#!/usr/bin/env bash

swift build && \
sourcekitten doc --spm-module Mercury > source.json && \
jazzy && \
rm source.json
