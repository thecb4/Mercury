#!/usr/bin/env bash

echo "=============== Running Swift on macOS (package) =============== " && \
rm -rf .build && swift package resolve && swift build && swift test && \
echo "=============== Running Swift on Linux (package) =============== " && \
docker run \
  --privileged --rm -t \
  --name swift-test \
  --volume "$(pwd):/package" \
  --workdir "/package" \
  swift:4.2 \
  /bin/bash -c \
  "rm -rf .build && swift package resolve && swift build && swift test --build-path ./.build/linux" && \
echo "=============== Running Swift on iOS (Carthage) ===============" && \
rm -rf Mercury.xcodeproj && rm -rf DerivedData && xcodegen generate && \
echo "=============== Building Carthage Dependencies =============== " && \
carthage bootstrap --platform ios && \
xcodebuild clean test -scheme "Mercury-iOS" -destination "platform=iOS Simulator,name=iPhone XS"
