# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.1.8] - 2019-Jan-19
### Added
- custom log level messenger log

### Changed (not fixed)
-

### Removed
-

### Fixed
-

## [0.1.7] - 2019-Jan-04
### Added
-

### Changed (not fixed)
-

### Removed
-

### Fixed
- HyperSpace 0.2.2 Service http status code method integration
